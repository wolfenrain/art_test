import 'dart:ui';

import 'package:art_test/grid_component.dart';
import 'package:art_test/main.dart';
import 'package:art_test/transport_component.dart';
import 'package:flame/components.dart';
import 'package:flame/extensions.dart';
import 'package:flame/geometry.dart';
import 'package:flame/sprite.dart';

class ItemComponent extends GridComponent
    with HasGameRef<ArtTest>, HasHitboxes, Collidable {
  ItemComponent({Vector2? gridPos})
      : super(gridPos: gridPos ?? Vector2.zero(), size: Vector2.all(16));

  @override
  int get priority => 99;

  double pixelsFromCenter(int pixels) {
    const pixelSize = 1 / 8;

    return 8 * pixelSize - pixels * pixelSize;
  }

  @override
  Future<void>? onLoad() async {
    await super.onLoad();

    final sheet = SpriteSheet(
      image: await gameRef.images.load('thingy.png'),
      srcSize: Vector2.all(16),
    );

    // addHitbox(HitboxRectangle());
    addHitbox(HitboxPolygon([
      Vector2(pixelsFromCenter(3), pixelsFromCenter(6)),
      Vector2(pixelsFromCenter(3), -pixelsFromCenter(7)),
      Vector2(-pixelsFromCenter(3), -pixelsFromCenter(7)),
      Vector2(-pixelsFromCenter(3), pixelsFromCenter(6)),
    ]));
    add(SpriteComponent(sprite: sheet.getSprite(6, 0)));
  }

  @override
  void renderTree(Canvas canvas) {
    super.renderTree(canvas);
    canvas.save();
    canvas.transform(transformMatrix.storage);
    final paint = Paint()
      ..color = Color(0xFFFF0000)
      ..style = PaintingStyle.stroke;
    renderHitboxes(canvas, paint: paint);
    canvas.restore();
  }

  Direction? previousDirection;
}
