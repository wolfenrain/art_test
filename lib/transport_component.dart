import 'dart:ui';

import 'package:art_test/grid_component.dart';
import 'package:art_test/item_component.dart';
import 'package:art_test/main.dart';
import 'package:flame/components.dart';
import 'package:flame/geometry.dart';
import 'package:flame/sprite.dart';
import 'package:flutter/material.dart';

enum Direction {
  up,
  down,
  left,
  right,
  downToRight,
  downToLeft,
  leftToDown,
  rightToDown,
  rightToUp,
  leftToUp,
  upToLeft,
  upToRight,
}

bool canConnect(Direction dir1, Direction dir2) {
  const options = <Direction, List<Direction>>{
    Direction.up: [Direction.up, Direction.upToLeft, Direction.upToRight],
    Direction.down: [
      Direction.down,
      Direction.downToLeft,
      Direction.downToRight,
    ],
    Direction.left: [Direction.left, Direction.leftToDown, Direction.leftToUp],
    Direction.right: [
      Direction.right,
      Direction.rightToDown,
      Direction.rightToUp,
    ],
    Direction.downToRight: [
      Direction.downToRight, // TODO: Should they connect?
      Direction.right,
      Direction.rightToDown,
      Direction.rightToUp,
    ],
    Direction.downToLeft: [
      Direction.downToLeft, // TODO: Should they connect?
      Direction.left,
      Direction.leftToDown,
      Direction.leftToUp,
    ],
    Direction.leftToDown: [
      Direction.leftToDown, // TODO: Should they connect?
      Direction.down,
      Direction.downToLeft,
      Direction.downToRight,
    ],
    Direction.rightToDown: [
      Direction.rightToDown, // TODO: Should they connect?
      Direction.down,
      Direction.downToLeft,
      Direction.downToRight,
    ],
    Direction.rightToUp: [
      Direction.rightToUp, // TODO: Should they connect?
      Direction.up,
      Direction.upToLeft,
      Direction.upToRight,
    ],
    Direction.leftToUp: [
      Direction.leftToUp, // TODO: Should they connect?
      Direction.up,
      Direction.upToLeft,
      Direction.upToRight,
    ],
    Direction.upToLeft: [
      Direction.upToLeft, // TODO: Should they connect?
      Direction.left,
      Direction.leftToDown,
      Direction.leftToUp,
    ],
    Direction.upToRight: [
      Direction.upToRight, // TODO: Should they connect?
      Direction.right,
      Direction.rightToDown,
      Direction.rightToUp,
    ],
  };
  return options[dir1]?.contains(dir2) ?? false;
}

enum TransportType {
  conveyer,
}

class TransportComponent extends GridComponent
    with HasGameRef<ArtTest>, HasHitboxes, Collidable {
  TransportComponent({
    required this.direction,
    this.type = TransportType.conveyer,
    Vector2? gridPos,
  }) : super(gridPos: gridPos);

  final TransportType type;

  final Direction direction;

  static const speed = 10;

  double pixelsFromCenter(int pixels) {
    const pixelSize = 1 / 8;

    return 8 * pixelSize - pixels * pixelSize;
  }

  HitboxShape get shape => hitboxes.first;

  @override
  Future<void>? onLoad() async {
    await super.onLoad();

    if (direction == Direction.left ||
        direction == Direction.downToRight ||
        direction == Direction.leftToUp ||
        direction == Direction.upToLeft ||
        direction == Direction.rightToDown) {
      flipHorizontallyAroundCenter();
    }
    if (direction == Direction.up) {
      flipVerticallyAroundCenter();
    }
    final sheet = SpriteSheet(
      image: await gameRef.images.load('thingy.png'),
      srcSize: size,
    );

    collidableType = CollidableType.passive;

    switch (direction) {
      case Direction.up:
      case Direction.down:
        addHitbox(HitboxPolygon([
          Vector2(-pixelsFromCenter(3), -pixelsFromCenter(0)),
          Vector2(pixelsFromCenter(3), -pixelsFromCenter(0)),
          Vector2(pixelsFromCenter(3), pixelsFromCenter(0)),
          Vector2(-pixelsFromCenter(3), pixelsFromCenter(0)),
        ]));
        break;
      case Direction.left:
      case Direction.right:
        addHitbox(HitboxPolygon([
          Vector2(-pixelsFromCenter(0), -pixelsFromCenter(3)),
          Vector2(pixelsFromCenter(0), -pixelsFromCenter(3)),
          Vector2(pixelsFromCenter(0), pixelsFromCenter(5)),
          Vector2(-pixelsFromCenter(0), pixelsFromCenter(5)),
        ]));
        break;
      case Direction.upToLeft:
      case Direction.upToRight:
      case Direction.leftToDown:
      case Direction.rightToDown:
        addHitbox(HitboxPolygon([
          Vector2(-pixelsFromCenter(4), -pixelsFromCenter(3)),
          Vector2(pixelsFromCenter(0), -pixelsFromCenter(3)),
          Vector2(pixelsFromCenter(0), pixelsFromCenter(5)),
          Vector2(pixelsFromCenter(3), pixelsFromCenter(5)),
          Vector2(pixelsFromCenter(3), pixelsFromCenter(0)),
          Vector2(-pixelsFromCenter(3), pixelsFromCenter(0)),
          Vector2(-pixelsFromCenter(3), -pixelsFromCenter(4)),
          Vector2(-pixelsFromCenter(4), -pixelsFromCenter(4)),
        ]));
        break;
      case Direction.downToRight:
      case Direction.downToLeft:
      case Direction.rightToUp:
      case Direction.leftToUp:
        addHitbox(HitboxPolygon([
          Vector2(pixelsFromCenter(3), -pixelsFromCenter(0)),
          Vector2(-pixelsFromCenter(3), -pixelsFromCenter(0)),
          Vector2(-pixelsFromCenter(3), -pixelsFromCenter(3)),
          Vector2(-pixelsFromCenter(0), -pixelsFromCenter(3)),
          Vector2(-pixelsFromCenter(0), pixelsFromCenter(5)),
          Vector2(pixelsFromCenter(5), pixelsFromCenter(5)),
          Vector2(pixelsFromCenter(5), pixelsFromCenter(6)),
          Vector2(pixelsFromCenter(4), pixelsFromCenter(6)),
          Vector2(pixelsFromCenter(4), pixelsFromCenter(7)),
          Vector2(pixelsFromCenter(3), pixelsFromCenter(7)),
        ]));
        break;
    }

    add(SpriteAnimationGroupComponent(
      size: size,
      animations: {
        Direction.up: animation(sheet, row: 0),
        Direction.down: animation(sheet, row: 0),
        Direction.left: animation(sheet, row: 1),
        Direction.right: animation(sheet, row: 1),
        Direction.downToRight: animation(sheet, row: 2),
        Direction.downToLeft: animation(sheet, row: 2),
        Direction.leftToDown: animation(sheet, row: 3),
        Direction.rightToDown: animation(sheet, row: 3),
        Direction.leftToUp: animation(sheet, row: 4),
        Direction.rightToUp: animation(sheet, row: 4),
        Direction.upToLeft: animation(sheet, row: 5),
        Direction.upToRight: animation(sheet, row: 5),
      },
      current: direction,
    ));
  }

  SpriteAnimation animation(SpriteSheet sheet, {required int row}) {
    return sheet.createAnimation(row: row, stepTime: .2, to: 4);
  }

  @override
  void renderTree(Canvas canvas) {
    super.renderTree(canvas);
    canvas.save();
    canvas.transform(transformMatrix.storage);
    final paint = Paint()
      ..color = Color(0xFFFF0000)
      ..style = PaintingStyle.stroke;
    renderHitboxes(canvas, paint: paint);
    canvas.restore();
  }

  final List<ItemComponent> transporting = [];

  @override
  void onCollision(Set<Vector2> intersectionPoints, Collidable other) {
    // TODO: add contains
    if (other is ItemComponent && !transporting.contains(other)) {
      print(other);
      transporting.add(other);
    }
  }

  @override
  void onCollisionEnd(Collidable other) {
    print(containsPoint(other.hitboxes.first.absoluteCenter));
    if (other is ItemComponent &&
        !containsPoint(other.hitboxes.first.absoluteCenter)) {
      // TODO: add contains
      print('remove');
      transporting.remove(other);
    }
  }

  @override
  void update(double dt) {
    // if (transporting.isEmpty) {
    //   transporting.addAll(gameRef.children
    //       .whereType<ItemComponent>()
    //       .where((c) => c.gridPos == gridPos));
    // }
    for (final item in transporting) {
      moveItem(item, dt);
    }
  }

  void moveItem(ItemComponent item, double dt) {
    // if (item.previousDirection != null) {
    //   if (!canConnect(item.previousDirection!, direction)) {
    //     print('cant connect ${item.previousDirection} to $direction');
    //     return;
    //   }
    // }
    // TODO: Should keep track if the direction is of the same component.
    // Otherwise u could technically jump "transport".
    if (item.previousDirection != direction) {
      item.previousDirection = direction;
    }

    if (direction == Direction.left) {
      item.position.x -= speed * dt;
    }
    if (direction == Direction.right) {
      item.position.x += speed * dt;
    }

    if (direction == Direction.leftToDown) {
      if (item.center.x > center.x) {
        item.position.x -= speed * dt;
      } else {
        item.position.y += speed * dt;
      }
    }
    if (direction == Direction.rightToDown) {
      if (item.center.x < center.x) {
        item.position.x += speed * dt;
      } else {
        item.position.y += speed * dt;
      }
    }

    if (direction == Direction.down) {
      item.position.y += speed * dt;
    }
    if (direction == Direction.up) {
      item.position.y -= speed * dt;
    }

    if (direction == Direction.downToRight) {
      if (item.center.y < center.y) {
        item.position.y += speed * dt;
      } else {
        item.position.x += speed * dt;
      }
    }
    if (direction == Direction.downToLeft) {
      if (item.center.y < center.y) {
        item.position.y += speed * dt;
      } else {
        item.position.x -= speed * dt;
      }
    }

    if (direction == Direction.rightToUp) {
      if (item.center.x < center.x) {
        item.position.x += speed * dt;
      } else {
        item.position.y -= speed * dt;
      }
    }
    if (direction == Direction.leftToUp) {
      if (item.center.x > center.x) {
        item.position.x -= speed * dt;
      } else {
        item.position.y -= speed * dt;
      }
    }

    if (direction == Direction.upToLeft) {
      if (item.center.y > center.y) {
        item.position.y -= speed * dt;
      } else {
        item.position.x -= speed * dt;
      }
    }

    if (direction == Direction.upToRight) {
      if (item.center.y > center.y) {
        item.position.y -= speed * dt;
      } else {
        item.position.x += speed * dt;
      }
    }
  }
}
