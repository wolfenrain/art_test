import 'package:flame/components.dart';

class GridComponent extends PositionComponent {
  GridComponent({
    Vector2? gridPos,
    Vector2? size,
  }) : super(
          size: size ?? Vector2.all(16),
          position: (gridPos ?? Vector2.zero()).clone()
            ..multiply(Vector2.all(16)),
        );

  double get right => position.x + center.x;

  Vector2 get gridPos => Vector2(
        ((center.x ~/ 16)).toDouble(),
        ((center.y ~/ 16)).toDouble(),
      );
}
