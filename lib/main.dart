import 'package:art_test/transport_component.dart';
import 'package:flame/components.dart';
import 'package:flame/game.dart';
import 'package:flame/input.dart';
import 'package:flutter/material.dart';

import 'item_component.dart';

void main() {
  runApp(GameWidget(game: ArtTest()));
}

class ArtTest extends FlameGame with HasCollidables, HasTappables {
  @override
  Color backgroundColor() => Colors.lightGreen;

  final width = 6;
  final height = 6;

  @override
  Future<void>? onLoad() async {
    await super.onLoad();

    const map = [
      null, null, null, null, null, null, //
      Direction.leftToDown, Direction.left, Direction.upToLeft, //
      Direction.upToRight, Direction.right, Direction.rightToDown, //
      Direction.down, null, Direction.up, Direction.up, null, Direction.down, //
      Direction.downToRight, Direction.rightToDown, Direction.up, Direction.up,
      Direction.leftToDown, Direction.downToLeft, //
      null, Direction.down, Direction.up, Direction.up, Direction.down, null, //
      null, Direction.downToRight, Direction.rightToUp, Direction.leftToUp,
      Direction.downToLeft, null, //
    ];

    for (var i = 0; i < map.length; i++) {
      final direction = map[i];
      if (direction == null) {
        continue;
      }
      final pos = Vector2((i % width).toDouble(), (i ~/ height).toDouble());
      add(TransportComponent(direction: direction, gridPos: pos));
    }
    add(ItemComponent(gridPos: Vector2(1, 1)));
    add(ItemComponent(gridPos: Vector2(4, 1)));
    add(ItemComponent(gridPos: Vector2(2, 1)));

    add(HudButtonComponent(
      margin: const EdgeInsets.only(left: 80, bottom: 60),
      size: Vector2.all(16),
      button: SpriteComponent(
        sprite: await loadSprite('thingy.png', srcSize: Vector2.all(16)),
      ),
    ));

    camera.zoom = 6;
    camera.followVector2(Vector2(64, 64));
  }
}
